Source: dma
Section: mail
Priority: optional
Maintainer: Arno Töll <arno@debian.org>
Uploaders: Laurent Bigonville <bigon@debian.org>, Simon Schubert <2@0x2c.org>
Build-Depends: byacc,
               debhelper-compat (= 13),
               flex,
               libssl-dev,
               po-debconf
Standards-Version: 4.6.1
Homepage: https://github.com/corecode/dma
Vcs-Git: https://salsa.debian.org/debian/dma.git
Vcs-Browser: https://salsa.debian.org/debian/dma
Rules-Requires-Root: binary-targets

Package: dma
Architecture: any
Depends: ucf (>= 0.28), ${misc:Depends}, ${shlibs:Depends}
Provides: mail-transport-agent
Conflicts: mail-transport-agent
Replaces: mail-transport-agent
Multi-Arch: foreign
Description: lightweight mail transport agent
 The DragonFly Mail Agent is a small Mail Transport Agent (MTA),
 designed for home and office use.  It accepts mails from local Mail
 User Agents (MUA) and delivers them either to local mailboxes or
 remote SMTP servers.  Remote delivery includes support for features
 such as TLS/SSL and SMTP authentication.
 .
 dma is not intended as a replacement for full-featured MTAs like
 Sendmail, Postfix, or Exim.  Consequently, dma does not listen on
 port 25 for incoming connections.
