dma (0.9-1) unstable; urgency=low

  This release drops support for the MAILNAMEFILE directive in dma.conf. The
  behavior was Debian specific, and caused confusion as the MAILNAME directive
  supersedes this behavior. Users of previous releases who rely on
  MAILNAMEFILE may reference the file location in MAILNAME instead.

  Moreover, we do not support dbounce-simple-safecat and dma-migrate anymore.
  This was a Debian specific addition which was rejected upstream. Thus, we
  removed both with this release. dma-migrate might not be needed anymore as
  hopefully all mails in the spool have been delivered since 2010 by now.
  The dbounce-simple-safecat behavior is removed as we considered the
  improvement not important enough to carry it out-of-tree in Debian only. You
  may want to remove the DBOUNCEPROG configuration option entirely from your
  dma.conf configuration.

 -- Arno Töll <arno@debian.org>  Sun, 07 Jul 2013 12:20:39 +0200

dma (0.0.2010.06.17-3) unstable; urgency=low

   The default delivery mode has been changed to immediate, as it is in
   the upstream version of dma; the DEFER keyword is now disabled by default
   in dma.conf.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 27 Jul 2010 13:26:48 +0300

dma (0.0.2010.06.17-1) unstable; urgency=low

   The dma spool directory format has changed.  The Debian package of dma now
   recommends a separate package containing the dma-migrate utility; if it is
   present, it will be invoked at each periodic dma queue flush and attempt to
   convert the existing old-style queued messages to the new format.  In most
   cases, this should not incur any performance penalties in normal operation,
   since dma-migrate will scan the spool directory and ignore any new messages
   (they should already be in the new format); however, if it appears that
   the periodic queue flush runs take longer than usual to start up, you may
   remove the dma-migrate package once you have ascertained that your queue
   directory (/var/spool/dma) only contains files with names beginning with
   the letters M or Q.

   This version of dma knows how to perform MX lookups, so remote delivery is
   now possible directly, not through a smarthost.  However, a smarthost setup
   might still be preferred on many systems for various reasons - e.g. dynamic
   address assignment, a central outgoing mailserver, a roaming laptop, etc.

 -- Peter Pentchev <roam@ringlet.net>  Mon, 21 Jun 2010 11:03:57 +0300

